package wybory.okregi;
// Justyna Micota 418427

import java.util.HashMap;

public class MetodaDHonta extends MetodaZliczania {
    // mapa przechowująca następny dzielnik partii używany w metodzie
    protected HashMap<String, Integer> dzielnikiIlorazowWyborczych;
    protected HashMap<String, Integer> liczbaGlosowPartii;
    protected HashMap<String, Integer> podzielonaLiczbaGlosow;

    public MetodaDHonta() {
        dzielnikiIlorazowWyborczych = new HashMap<>();
        liczbaGlosowPartii = new HashMap<>();
        podzielonaLiczbaGlosow = new HashMap<>();
    }

    private void dodajPartie(String nazwaPartii, int liczbaGlosow) {
        dzielnikiIlorazowWyborczych.put(nazwaPartii, 1);
        liczbaGlosowPartii.put(nazwaPartii, liczbaGlosow);
        podzielonaLiczbaGlosow.put(nazwaPartii, liczbaGlosow);
    }

    // dodaje wszystkie partie wraz z liczbą uzyskanych przez nie głosów
    protected void wypelnijTabliceDanych(Okregi okregi, int nrOkregu) {
        wyczyscTabliceDanych();

        OkragWyborczy okrag1 = okregi.getOkragWyborczy(nrOkregu);

        // scalony z innym
        if (okrag1.getZKtorymScalony() != nrOkregu) {
            OkragWyborczy okrag2 = okregi.getOkragWyborczy(nrOkregu + 1);
            HashMap<String, ListaKandydatow> listyKandydatow1 = okrag1.getListyKandydatow();
            HashMap<String, ListaKandydatow> listyKandydatow2 = okrag2.getListyKandydatow();

            for (HashMap.Entry<String, ListaKandydatow> entry : listyKandydatow1.entrySet()) {
                String nazwaPartii = entry.getKey();
                ListaKandydatow listaKandydatow1 = entry.getValue();
                ListaKandydatow listaKandydatow2 = listyKandydatow2.get(nazwaPartii);
                int liczbaGlosow = listaKandydatow1.liczGlosyNaLiscie() + listaKandydatow2.liczGlosyNaLiscie();
                dodajPartie(nazwaPartii, liczbaGlosow);
            }
        }
        else { // niescalony
            HashMap<String, ListaKandydatow> listyKandydatow = okrag1.getListyKandydatow();

            for (HashMap.Entry<String, ListaKandydatow> entry : listyKandydatow.entrySet()) {
                String nazwaPartii = entry.getKey();
                ListaKandydatow listaKandydatow = entry.getValue();
                int liczbaGlosow = listaKandydatow.liczGlosyNaLiscie();
                dodajPartie(nazwaPartii, liczbaGlosow);
            }
        }
    }

    // metoda zapewniajaca poprawne działanie algorytmu dla każdego zliczanego okręgu
    private void wyczyscTabliceDanych() {
        dzielnikiIlorazowWyborczych = new HashMap<>();
        liczbaGlosowPartii = new HashMap<>();
        podzielonaLiczbaGlosow = new HashMap<>();
    }

    // zwiększa wartość dzielnika dla partii zgodnie z zasadą działania metody D'Honta
    public void zwiekszDzielnik(String nazwaPartii) {
        dzielnikiIlorazowWyborczych.replace(nazwaPartii, dzielnikiIlorazowWyborczych.get(nazwaPartii) + 1);
    }

    // znajduje następną partię o maksymalnej liczbie głosów dzielonej wcześniej przez odpowiednie dzielniki
    // zwraca nazwę tej partii
    private String nastepnaMaksymalnaPartia() {
        int maksimum = -1;
        String nazwaPartii = "";
        for (HashMap.Entry<String, Integer> entry : podzielonaLiczbaGlosow.entrySet()) {
            if (entry.getValue() > maksimum ) {
                nazwaPartii = entry.getKey();
                maksimum = podzielonaLiczbaGlosow.get(nazwaPartii);
            }
        }
        return nazwaPartii;
    }

    // aktualizuje (odpowiednio zwiększa) dzielnik partii
    private void podzielGlosyPartii(String nazwaPartii) {
        int liczbaGlosow = liczbaGlosowPartii.get(nazwaPartii);
        zwiekszDzielnik(nazwaPartii);
        liczbaGlosow = liczbaGlosow/dzielnikiIlorazowWyborczych.get(nazwaPartii);
        podzielonaLiczbaGlosow.replace(nazwaPartii, liczbaGlosow);
    }

    @Override
    public void przydzielMandatyWOkregu(Okregi okregi, int nrOkregu) {
        wypelnijTabliceDanych(okregi, nrOkregu);
        OkragWyborczy okragWyborczy = okregi.getOkragWyborczy(nrOkregu);

        int liczbaMandatow = okragWyborczy.liczbaMandatow();
        if (okragWyborczy.getZKtorymScalony() != nrOkregu) {
            liczbaMandatow += okregi.getOkragWyborczy(nrOkregu + 1).liczbaMandatow();
        }

        for (int i = 0; i < liczbaMandatow; i++) {
            String nastepnaPartia = nastepnaMaksymalnaPartia();
            okragWyborczy.dodajMandatyPartii(nastepnaPartia, 1);
            podzielGlosyPartii(nastepnaPartia);
        }
    }

    @Override
    public String toString() {
        return "Metoda D'Honta\n";
    }
}
