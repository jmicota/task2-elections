package wybory.okregi;
// Justyna Micota 418427

import wybory.partie.Partia;
import wybory.partie.PojedynczeDzialanie;
import wybory.partie.SpisPartii;
import wybory.wyborca.Wyborca;

import java.util.ArrayList;
import java.util.HashMap;

// klasa implementująca metody operujące wewnątrz danego okręgu wyborczego
public class OkragWyborczy {
    private final int idOkregu;
    private final int liczbaWyborcow;
    private final HashMap<String, ListaKandydatow> listyKandydatow;
    private final HashMap<String, Integer> mandatyPartii;
    private final ArrayList<Wyborca> wyborcy;
    private int zKtorymScalony; // dla niescalonego okręgu jest równa idOkregu

    public OkragWyborczy(int n, int idOkregu, SpisPartii partie) {
        this.liczbaWyborcow = n;
        this.idOkregu = idOkregu;
        listyKandydatow = new HashMap<>();
        mandatyPartii = new HashMap<>();
        HashMap<String, Partia> spisPartii = partie.getSpisPartii();
        for (HashMap.Entry<String, Partia> entry : spisPartii.entrySet()) {
            String nazwa = entry.getKey();
            listyKandydatow.put(nazwa, new ListaKandydatow());
            mandatyPartii.put(nazwa, 0);
        }
        wyborcy = new ArrayList<>();
        zKtorymScalony = idOkregu;
    }

    public void resetujLiczbeMandatow() {
        for (HashMap.Entry<String, Integer> entry : mandatyPartii.entrySet()) {
            String nazwa = entry.getKey();
            mandatyPartii.replace(nazwa, 0);
        }
    }

    public ArrayList<Wyborca> getWyborcy() {
        return wyborcy;
    }

    public int liczbaWyborcow() {
        return wyborcy.size();
    }

    public HashMap<String, ListaKandydatow> getListyKandydatow() {
        return listyKandydatow;
    }

    public HashMap<String, Integer> getMandatyPartii() {
        return mandatyPartii;
    }

    public int getIdOkregu() {
        return this.idOkregu;
    }

    public void dodajKandydata(Kandydat kandydat, String nazwaPartii) {
        listyKandydatow.get(nazwaPartii).dodajKandydata(kandydat);
    }

    public void dodajWyborce(Wyborca wyborca) {
        wyborcy.add(wyborca);
    }

    public void dodajMandatyPartii(String nazwaPartii, int ile) {
        int aktualnaLiczbaMandatow = mandatyPartii.get(nazwaPartii);
        mandatyPartii.replace(nazwaPartii, aktualnaLiczbaMandatow + ile);
    }

    public int liczbaMandatowPartii(String nazwaPartii) {
        return mandatyPartii.get(nazwaPartii);
    }

    public int getZKtorymScalony() {
        return zKtorymScalony;
    }

    // zwraca liczbę mandatów możliwych do uzyskania w tym okręgu
    public int liczbaMandatow() {
        return liczbaWyborcow/10;
    }

    public void zapiszZKtorymScalony(int zKtorym) {
        zKtorymScalony = zKtorym;
    }

    public void poddajOkragDzialaniu(PojedynczeDzialanie dzialanie) {
        for (Wyborca wyborca : wyborcy) {
            wyborca.poddajSieDzialaniu(dzialanie);
        }
    }

    public void wypiszWyborcowOkregu() {
        for (Wyborca wyborca : wyborcy) {
            System.out.print(wyborca.toString());
        }
    }

    public void wypiszKandydatowOkregu() {
        for (HashMap.Entry<String, ListaKandydatow> entry : listyKandydatow.entrySet()) {
            ListaKandydatow listaKandydatow = entry.getValue();
            listaKandydatow.wypiszWynikiKandydatow();
        }
    }

    public void wypiszWynikiPartiiWOKregu() {
        for (HashMap.Entry<String, Integer> entry : mandatyPartii.entrySet()) {
            int liczbaMandatow = entry.getValue();
            System.out.println(entry.getKey() + " " + liczbaMandatow);
        }
    }
}
