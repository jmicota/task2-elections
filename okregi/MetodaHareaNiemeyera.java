package wybory.okregi;
// Justyna Micota 418427

import java.util.HashMap;

public class MetodaHareaNiemeyera extends MetodaDHonta {
    // mapa przechowująca wartości reszt dla partii, zgodnie z zasadą działania metody Hare'a-Niemeyera
    private final HashMap<String, Double> reszty;

    public MetodaHareaNiemeyera() {
        super();
        reszty = new HashMap<>();
    }

    // znajduje partię o największej reszcie
    // zwraca jej nazwę
    private String maksymalnaReszta() {
        double maksimum = 0;
        String nazwaPartii = "";
        for (HashMap.Entry<String, Double> entry : reszty.entrySet()) {
            String nazwa = entry.getKey();
            if (entry.getValue() > maksimum ) {
                nazwaPartii = nazwa;
                maksimum = entry.getValue();
            }
        }
        return nazwaPartii;
    }

    // przydziela pozostałe po wykonaniu głównej części algorytmu mandaty zgodnie
    // z zasadą działania metody Hare'a-Niemeyera
    private void przydzielReszteMandatow(int pozostaleMandaty, OkragWyborczy okragWyborczy) {
        for (int i = 0; i < pozostaleMandaty; i++) {
            String partiaOMaksymalnejReszcie = maksymalnaReszta();
            okragWyborczy.dodajMandatyPartii(partiaOMaksymalnejReszcie, 1);
            reszty.replace(partiaOMaksymalnejReszcie, 0.0);
        }
    }

    @Override
    public void przydzielMandatyWOkregu(Okregi okregi, int nrOkregu) {
        liczbaGlosowPartii = new HashMap<>();
        wypelnijTabliceDanych(okregi, nrOkregu);
        OkragWyborczy okragWyborczy = okregi.getOkragWyborczy(nrOkregu);

        int pozostaleMandaty = okragWyborczy.liczbaMandatow();
        if (okragWyborczy.getZKtorymScalony() != nrOkregu) {
            pozostaleMandaty += okregi.getOkragWyborczy(nrOkregu + 1).liczbaMandatow();
        }

        for (HashMap.Entry<String, Integer> entry : liczbaGlosowPartii.entrySet()) {
            int liczbaGlosow = entry.getValue();
            int liczbaMandatowPartii = liczbaGlosow / 10;

            Double reszta = (double)(liczbaGlosow % 10);
            reszty.put(entry.getKey(), reszta);

            okragWyborczy.dodajMandatyPartii(entry.getKey(), liczbaMandatowPartii);
            pozostaleMandaty -= liczbaMandatowPartii;
        }
        przydzielReszteMandatow(pozostaleMandaty, okragWyborczy);
    }

    @Override
    public String toString() {
        return "Metoda Hare'a-Niemeyera";
    }
}