package wybory.okregi;
// Justyna Micota 418427

import wybory.partie.SpisPartii;
import wybory.wyborca.Wyborca;

import java.util.ArrayList;
import java.util.HashMap;

// klasa implementująca metody operujące na wielu okręgach wyborczych
public class Okregi {
    ArrayList<OkragWyborczy> okregi;

    // zerowy okrąg wyborczy zapewnia możliwośc odwoływania się do okręgów za pomocą
    // indeksowania od jedynki
    public Okregi(SpisPartii partie) {
        okregi = new ArrayList<>();
        OkragWyborczy zerowy = new OkragWyborczy(0, 0, partie);
        okregi.add(zerowy);
    }

    public void dodajOkrag(OkragWyborczy okragWyborczy) {
        okregi.add(okragWyborczy);
    }

    public OkragWyborczy getOkragWyborczy(int i) {
        return okregi.get(i);
    }

    public int liczbaOkregow() {
        return okregi.size() - 1;
    }

    public int nrNajwiekszegoOkregu() {
        int maksymalnaLiczbaWyborcow = -1;
        int nrOkregu = -1;
        for (int i = 1; i < okregi.size(); i++) {
            int liczbaWyborcow = okregi.get(i).liczbaWyborcow();
            if (liczbaWyborcow > maksymalnaLiczbaWyborcow) {
                maksymalnaLiczbaWyborcow = liczbaWyborcow;
                nrOkregu = i;
            }
        }
        return nrOkregu;
    }

    public int nrNajmniejszegoOkregu() {
        int maksymalnyOkrag = nrNajwiekszegoOkregu();
        int minimalnaLiczbaWyborcow = okregi.get(maksymalnyOkrag).liczbaWyborcow();
        int nrOkregu = maksymalnyOkrag;

        for (int i = 1; i < okregi.size(); i++) {
            int liczbaWyborcow = okregi.get(i).liczbaWyborcow();
            if (liczbaWyborcow < minimalnaLiczbaWyborcow) {
                minimalnaLiczbaWyborcow = liczbaWyborcow;
                nrOkregu = i;
            }
        }
        return nrOkregu;
    }

    // metoda przeprowadzająca głosowanie dla pojedynczego okręgu
    private void glosowanieDlaPojedynczegoOkregu(int nrOkregu) {
        OkragWyborczy okragWyborczy = okregi.get(nrOkregu);
        ArrayList<Wyborca> wyborcy = okragWyborczy.getWyborcy();
        for (Wyborca wyborca : wyborcy) {
            wyborca.zaglosuj(okragWyborczy);
        }
    }

    // metoda przeprowadzająca głosowanie dla scalonych okręgów
    private void glosowanieDlaPolaczonychOkregow(int nrPierwszegoOkregu) {
        OkragWyborczy pierwszyOkrag = okregi.get(nrPierwszegoOkregu);
        OkragWyborczy drugiOkrag = okregi.get(nrPierwszegoOkregu + 1);

        ArrayList<Wyborca> wyborcyPierwszego = pierwszyOkrag.getWyborcy();
        ArrayList<Wyborca> wyborcyDrugiego = drugiOkrag.getWyborcy();

        for (Wyborca wyborca : wyborcyPierwszego) {
            wyborca.zaglosujZDwoch(pierwszyOkrag, drugiOkrag);
        }
        for (Wyborca wyborca : wyborcyDrugiego) {
            wyborca.zaglosujZDwoch(pierwszyOkrag, drugiOkrag);
        }
    }

    // metoda przeprowadzająca głosowanie dla wszystkich okręgów
    public void przeprowadzGlosowanie() {
        for (int i = 1; i < liczbaOkregow() + 1; i++) {
            int zKtorymScalony = okregi.get(i).getZKtorymScalony();
            if (zKtorymScalony != i) {
                glosowanieDlaPolaczonychOkregow(i);
                i++;
            }
            else {
                glosowanieDlaPojedynczegoOkregu(i);
            }
        }
    }

    // zwraca liczbę mandatów partii ze wszystkich okręgów
    private int liczWszystkieMandatyPartii(String nazwaPartii) {
        int suma = 0;
        for (int i = 1; i < liczbaOkregow() + 1; i++) {
            suma += okregi.get(i).liczbaMandatowPartii(nazwaPartii);
        }
        return suma;
    }

    public void resetujMandaty() {
        for (int i = 1; i < liczbaOkregow() + 1; i++) {
            okregi.get(i).resetujLiczbeMandatow();
        }
    }

    private void wypiszWynikiPojedynczego(int nrOkregu) {
        System.out.println("NR OKREGU: " + nrOkregu);
        System.out.println("WYBORCY:");
        okregi.get(nrOkregu).wypiszWyborcowOkregu();
        System.out.println("KANDYDACI:");
        okregi.get(nrOkregu).wypiszKandydatowOkregu();
        System.out.println("PARTIE:");
        okregi.get(nrOkregu).wypiszWynikiPartiiWOKregu();
        System.out.println();
    }

    private void wypiszWynikiScalonych(int nrOkregu) {
        System.out.println("NR OKREGOW: " + nrOkregu + " " + (nrOkregu + 1));
        System.out.println("WYBORCY:");
        okregi.get(nrOkregu).wypiszWyborcowOkregu();
        okregi.get(nrOkregu + 1).wypiszWyborcowOkregu();
        System.out.println("KANDYDACI:");
        okregi.get(nrOkregu).wypiszKandydatowOkregu();
        okregi.get(nrOkregu + 1).wypiszKandydatowOkregu();
        System.out.println("PARTIE:");
        HashMap<String, Integer> mandatyZPierwszego = okregi.get(nrOkregu).getMandatyPartii();
        HashMap<String, Integer> mandatyZDrugiego = okregi.get(nrOkregu + 1).getMandatyPartii();
        for (HashMap.Entry<String, Integer> entry : mandatyZPierwszego.entrySet()) {
            String nazwaPartii = entry.getKey();
            System.out.println(nazwaPartii + " " + (entry.getValue() + mandatyZDrugiego.get(nazwaPartii)));
        }
        System.out.println();
    }

    public void wypiszWyniki() {
        for (int i = 1; i < liczbaOkregow() + 1; i++) {
            int zKtorymScalony = okregi.get(i).getZKtorymScalony();
            if (zKtorymScalony == i) {
                wypiszWynikiPojedynczego(i);
            }
            else {
                wypiszWynikiScalonych(i);
                i++;
            }
        }
        System.out.println("DLA WSZYSTKICH OKREGOW:");
        HashMap<String, ListaKandydatow> listyKandydatow;
        if (liczbaOkregow() > 0) {
            listyKandydatow = okregi.get(1).getListyKandydatow();
            for (HashMap.Entry<String, ListaKandydatow> entry : listyKandydatow.entrySet()) {
                String nazwaPartii = entry.getKey();
                System.out.println(nazwaPartii + " " + liczWszystkieMandatyPartii(nazwaPartii));
            }
        }
        System.out.println();
        System.out.println();
    }

}
