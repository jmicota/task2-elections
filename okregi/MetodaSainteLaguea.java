package wybory.okregi;
// Justyna Micota 418427

public class MetodaSainteLaguea extends MetodaDHonta {

    public MetodaSainteLaguea() {
        super();
    }

    // zwiększa wartość dzielnika dla partii zgodnie z zasadą działania metody Sainte-Lague'a
    @Override
    public void zwiekszDzielnik(String nazwaPartii) {
        int obecnyDzielnik = dzielnikiIlorazowWyborczych.get(nazwaPartii);
        dzielnikiIlorazowWyborczych.replace(nazwaPartii, obecnyDzielnik + 2);
    }

    @Override
    public String toString() {
        return "Metoda Sainte-Lague'a";
    }
}
