package wybory.okregi;
// Justyna Micota 418427

public class Kandydat {
    private final String imie;
    private final String nazwisko;
    private final String nazwaPartii;
    private final int nrNaLiscie;
    private final int[] cechy;
    private int otrzymaneGlosy;

    public Kandydat(String imie, String nazwisko, String
                    nazwaPartii, int nrNaLiscie, int[] cechy) {
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.nazwaPartii = nazwaPartii;
        this.nrNaLiscie = nrNaLiscie;
        this.cechy = new int[cechy.length];
        System.arraycopy(cechy, 0, this.cechy, 0, cechy.length);
        this.otrzymaneGlosy = 0;
    }

    public String getImie() {
        return imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public int getOtrzymaneGlosy() {
        return otrzymaneGlosy;
    }

    public int[] getCechy() {
        return cechy;
    }

    public void dodajGlos() {
        otrzymaneGlosy += 1;
    }

    public String toString() {
        return (imie + " " + nazwisko + " " + nazwaPartii + " " + nrNaLiscie + " " + otrzymaneGlosy + "\n");
    }
}
