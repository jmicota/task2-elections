package wybory.okregi;
// Justyna Micota 418427

// klasa abstraksyjna wymuszająca zgodność implementacji różnych metod zliczania
public abstract class MetodaZliczania {

    // metoda przydzielająca odpowiednio wyliczoną liczbę mandatów partiom
    // ta liczba zapisywana jest w mapie przechowywanej w odpowiednim okręgu wyborczym
    public abstract void przydzielMandatyWOkregu(Okregi okregi, int nrOkregu);

    public void przydzielMandaty(Okregi okregi) {
        for (int i = 1; i < okregi.liczbaOkregow() + 1; i++) {
            przydzielMandatyWOkregu(okregi, i);
            // jeśli był scalony przeskakujemy następny okrąg
            if (okregi.getOkragWyborczy(i).getZKtorymScalony() != i) {
                i++;
            }
        }
    }

    public abstract String toString();
}
