package wybory.okregi;
// Justyna Micota 418427

import java.util.ArrayList;

public class ListaKandydatow {
    private final ArrayList<Kandydat> kandydaciPartii;

    // przy inicjacji listy kandydat zerowy zapewnia możliwość odwoływania się
    // do kandydatów według narzuconego schematu indeksów rozpoczynających się od 1
    public ListaKandydatow() {
        kandydaciPartii = new ArrayList<>();
        Kandydat zerowy = new Kandydat("", "", "", 0, new int[0]);
        kandydaciPartii.add(zerowy);
    }

    public Kandydat getKandydat(int i) {
        return kandydaciPartii.get(i);
    }

    public int dlugoscListy() {
        return kandydaciPartii.size();
    }

    public int liczGlosyNaLiscie() {
        int suma = 0;
        for (Kandydat kandydat : kandydaciPartii) {
            suma += kandydat.getOtrzymaneGlosy();
        }
        return suma;
    }

    public void dodajKandydata(Kandydat kandydat) {
        kandydaciPartii.add(kandydat);
    }

    public void wypiszWynikiKandydatow() {
        for (int i = 1; i < kandydaciPartii.size(); i++) {
            System.out.print(kandydaciPartii.get(i).toString());
        }
    }
}
