package wybory;
// Justyna Micota 418427

import wybory.okregi.*;
import wybory.partie.Dzialania;
import wybory.partie.SpisPartii;

import java.util.Random;

// interfejs przeprowadzający symulację wyborów
public class Symulacja {
    private final Okregi okregi;
    private final SpisPartii spisPartii;
    private final Dzialania dzialania;


    public Symulacja(Okregi okregi, Dzialania dzialania, SpisPartii spisPartii) {
        this.okregi = okregi;
        this.dzialania = dzialania;
        this.spisPartii = spisPartii;
    }

    private String losujMetode() {
        String nazwaMetody = null;
        Random random = new Random();
        int n = random.nextInt(3);
        switch(n) {
            case 0: nazwaMetody = (new MetodaDHonta()).toString();
                    break;
            case 1: nazwaMetody = (new MetodaHareaNiemeyera()).toString();
                    break;
            case 2: nazwaMetody = (new MetodaSainteLaguea()).toString();
                    break;
        }
        return nazwaMetody;
    }

    public void symuluj() {
        MetodaZliczania dhonta = new MetodaDHonta();
        MetodaZliczania sainte = new MetodaSainteLaguea();
        MetodaZliczania harea = new MetodaHareaNiemeyera();

        String nazwaWylosowanejMetody = losujMetode();
        System.out.println("Wylosowana metoda: " + nazwaWylosowanejMetody);
        System.out.println();

        spisPartii.kampaniaWszystkichPartii(okregi, dzialania);
        okregi.przeprowadzGlosowanie();

        System.out.println("----> METODA D'HONTA <----");
        dhonta.przydzielMandaty(okregi);
        okregi.wypiszWyniki();
        okregi.resetujMandaty();

        System.out.println("----> METODA SAINTE-LAGUE'A <----");
        sainte.przydzielMandaty(okregi);
        okregi.wypiszWyniki();
        okregi.resetujMandaty();

        System.out.println("----> METODA HARE'A-NIEMEYERA <----");
        harea.przydzielMandaty(okregi);
        okregi.wypiszWyniki();
        okregi.resetujMandaty();
    }
}
