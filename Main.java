package wybory;
//Justyna Micota 418427

/*
 *  Dane pobierane są z pliku deklarowanego jak poniżej,
 *  Zamiast średnich ważonych korzystam z sum ważonych,
 *  Głosowanie przeprowadzam raz, następnie zliczam mandaty na trzy różne sposoby,
 */

import java.io.File;
import java.io.FileNotFoundException;

public class Main {

    public static void main(String[] args) throws FileNotFoundException {
        File file = new File("wybory/dane.txt");
        Dane dane = new Dane(file);
        Symulacja symulacja = dane.pobierzDane();
        symulacja.symuluj();
    }
}
