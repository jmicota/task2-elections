package wybory.partie;
// Justyna Micota 418427

import wybory.okregi.OkragWyborczy;
import wybory.okregi.Okregi;

public class PartiaSkromna extends Partia{

    public PartiaSkromna(String nazwa, int budzet) {
        super(nazwa, budzet);
    }

    @Override
    public void przeprowadzKampanie(Dzialania dzialania, Okregi okregi) {
        int liczbaDzialan = dzialania.liczbaDzialan();
        OkragWyborczy najtanszy = okregi.getOkragWyborczy(okregi.nrNajmniejszegoOkregu());
        int minimalnyKoszt;
        int wybraneDzialanie = 0;

        if (liczbaDzialan > 0) {
            minimalnyKoszt = dzialania.policzKosztDzialania(0, najtanszy);

            for (int i = 1; i < liczbaDzialan; i++) {
                int koszt = dzialania.getDzialanieNr(i).policzKoszt(najtanszy);
                if (koszt < minimalnyKoszt) {
                    minimalnyKoszt = koszt;
                    wybraneDzialanie = i;
                }
            }

            if (minimalnyKoszt > 0) {
                while (budzet > minimalnyKoszt) {
                    najtanszy.poddajOkragDzialaniu(dzialania.getDzialanieNr(wybraneDzialanie));
                    budzet =- minimalnyKoszt;
                }
            }
        }
    }
}
