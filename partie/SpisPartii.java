package wybory.partie;
// Justyna Micota 418427

import wybory.okregi.Okregi;

import java.util.HashMap;

public class SpisPartii {
    private final HashMap<String, Partia> partie;

    public SpisPartii() {
        partie = new HashMap<>();
    }

    public HashMap<String, Partia> getSpisPartii() {
        return this.partie;
    }

    public void dodajPartie(Partia partia) {
        partie.put(partia.getNazwaPartii(), partia);
    }

    public void kampaniaWszystkichPartii(Okregi okregi, Dzialania dzialania) {
        for (HashMap.Entry<String, Partia> entry : partie.entrySet()) {
            String nazwaPartii = entry.getKey();
            Partia partia = partie.get(nazwaPartii);
            partia.przeprowadzKampanie(dzialania, okregi);
        }
    }
}
