package wybory.partie;
// Justyna Micota 418427

import wybory.okregi.Okregi;

public abstract class Partia {
    private final String nazwa;
    protected int budzet;

    public Partia(String nazwa, int budzet) {
        this.nazwa = nazwa;
        this.budzet = budzet;
    }

    public String getNazwaPartii() {
        return nazwa;
    }

    abstract public void przeprowadzKampanie(Dzialania dzialania, Okregi okregi);
}
