package wybory.partie;
// Justyna Micota 418427

import wybory.okregi.OkragWyborczy;

public class PojedynczeDzialanie {
    private final int[] wartosciZmian;

    public PojedynczeDzialanie(int[] wartosciZmian) {
        this.wartosciZmian = wartosciZmian;
    }

    public int[] getWartosciZmian() {
        return wartosciZmian;
    }

    public int policzKoszt(OkragWyborczy okragWyborczy) {
        int liczbaWyborcow = okragWyborczy.liczbaMandatow() * 10;
        int sumaWartosciZmian = 0;

        for (int value : wartosciZmian) {
            sumaWartosciZmian += Math.abs(value);
        }

        return sumaWartosciZmian * liczbaWyborcow;
    }
}
