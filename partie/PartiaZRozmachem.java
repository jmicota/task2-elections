package wybory.partie;
// Justyna Micota 418427

import wybory.okregi.OkragWyborczy;
import wybory.okregi.Okregi;

public class PartiaZRozmachem extends Partia {
    private OkragWyborczy wybranyOkrag;
    private int wybranyKoszt;
    private PojedynczeDzialanie wybraneDzialanie;

    public PartiaZRozmachem(String nazwa, int budzet) {
        super(nazwa, budzet);
        // przechowuje aktualny najlepszy wybór
        wybranyOkrag = null;
        wybranyKoszt = 0;
        wybraneDzialanie = null;
    }

    // znajduje największy okrąg dla danego działania, na który stać partię
    // jeśli koszt jest większy niż wyboru zapisanego do tej pory, jest on nadpisywany
    private void zapiszWyborWedlugDzialania(int nrDzialania, Okregi okregi, Dzialania dzialania) {
        int liczbaOkregow = okregi.liczbaOkregow();
        PojedynczeDzialanie dzialanie = dzialania.getDzialanieNr(nrDzialania);

        for (int i = 1; i < liczbaOkregow + 1; i++) {
            int koszt = dzialanie.policzKoszt(okregi.getOkragWyborczy(i));
            if (koszt > wybranyKoszt && koszt <= budzet) {
                wybranyKoszt = koszt;
                wybraneDzialanie = dzialanie;
                wybranyOkrag = okregi.getOkragWyborczy(i);
            }
        }
    }

    private void zapiszWybor(Okregi okregi, Dzialania dzialania) {
        for (int i = 0; i < dzialania.liczbaDzialan(); i++) {
            zapiszWyborWedlugDzialania(i, okregi, dzialania);
        }
    }

    // wykonuje wybrane działanie dopóki starcza na nie pieniędzy w budżecie
    @Override
    public void przeprowadzKampanie(Dzialania dzialania, Okregi okregi) {
        zapiszWybor(okregi, dzialania);

        while (budzet >= wybranyKoszt) {
            wybranyOkrag.poddajOkragDzialaniu(wybraneDzialanie);
            budzet =- wybranyKoszt;
        }
    }
}
