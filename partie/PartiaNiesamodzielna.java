package wybory.partie;
// Justyna Micota 418427

import wybory.okregi.Okregi;

import java.util.Random;

// klasa implementująca własną strategię partii
// partia niesamodzielna losuje czyją strategię skopiuje w kampanii,
// ponieważ nie jest w stanie wymyślić własnej, ze względu na niekompetencję zarządu
public class PartiaNiesamodzielna extends Partia {

    public PartiaNiesamodzielna(String nazwa, int budzet) {
        super(nazwa, budzet);
    }

    @Override
    public void przeprowadzKampanie(Dzialania dzialania, Okregi okregi) {
        Partia partiaDoKopiowania = null;
        Random random = new Random();
        int typ = random.nextInt(3);
        switch (typ) {
            case 0: partiaDoKopiowania = new PartiaSkromna(getNazwaPartii(), budzet);
                    break;
            case 1: partiaDoKopiowania =  new PartiaZRozmachem(getNazwaPartii(), budzet);
                    break;
            case 2: partiaDoKopiowania =  new PartiaZachlanna(getNazwaPartii(), budzet);
                    break;
        }
        assert partiaDoKopiowania != null;
        partiaDoKopiowania.przeprowadzKampanie(dzialania, okregi);
    }
}
