package wybory.partie;
// Justyna Micota 418427

import wybory.okregi.OkragWyborczy;

// klasa zawierająca tablicę wszystkich dostępnych pojedynczych działań oraz
// implementująca ogólne metody operujące na tej tablicy, takie jak dodawanie
// nowego działania lub dostęp do wartości kosztu wybranego działania
public class Dzialania {

    PojedynczeDzialanie[] dzialania;

    public Dzialania(int liczbaDzialan) {
        dzialania = new PojedynczeDzialanie[liczbaDzialan];
    }

    public void dodajDzialanie(int i, PojedynczeDzialanie dzialanie) {
        dzialania[i] = dzialanie;
    }

    // zwraca koszt działania na danym okręgu wyborczym
    public int policzKosztDzialania(int i, OkragWyborczy okragWyborczy) {
        return dzialania[i].policzKoszt(okragWyborczy);
    }

    public PojedynczeDzialanie getDzialanieNr(int i) {
        if (i >= 0 && i < dzialania.length) {
            return dzialania[i];
        }
        else{
            System.out.println("getdzialanienr out of bound");
            return null;
        }
    }

    public int liczbaDzialan() {
        return dzialania.length;
    }
}
