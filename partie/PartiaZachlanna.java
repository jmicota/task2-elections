package wybory.partie;
// Justyna Micota 418427

import wybory.okregi.ListaKandydatow;
import wybory.okregi.OkragWyborczy;
import wybory.okregi.Okregi;
import wybory.wyborca.Wyborca;

import java.util.ArrayList;

public class PartiaZachlanna extends Partia {
    // przechowuje aktualny najlepszy wybór
    private OkragWyborczy wybranyOkrag;
    private int wybranaZmianaSumyWazonej;
    private PojedynczeDzialanie wybraneDzialanie;

    public PartiaZachlanna(String nazwa, int budzet) {
        super(nazwa, budzet);
        wybraneDzialanie = null;
        wybranaZmianaSumyWazonej = -1;
        wybranyOkrag = null;
    }

    // resetuje wybór po wykonaniu jednego działania
    private void resetujWybor() {
        wybranyOkrag = null;
        wybraneDzialanie = null;
        wybranaZmianaSumyWazonej = -1;
    }

    private int liczZmianeSumWazonych(OkragWyborczy okragWyborczy, String nazwaPartii, PojedynczeDzialanie dzialanie) {
        ListaKandydatow listaKandydatow = okragWyborczy.getListyKandydatow().get(nazwaPartii);
        ArrayList<Wyborca> wyborcy = okragWyborczy.getWyborcy();
        int suma = 0;

        for (int k = 0; k < listaKandydatow.dlugoscListy(); k++) {
            for (Wyborca wyborca : wyborcy) {
                suma += wyborca.liczZmianeSumyWazonej(listaKandydatow.getKandydat(k), dzialanie);
            }
        }
        return suma;
    }

    // porównuje potencjalne rezultaty jednego z działań na wszystkich okręgach
    // jeśli jest ono lepsze od poprzednio wybranego zapisuje je w atrybutach klasy
    private void sprawdzJednoDzialanie(Okregi okregi, PojedynczeDzialanie dzialanie) {
        int liczbaOkregow = okregi.liczbaOkregow();

        for (int i = 1; i < liczbaOkregow + 1; i++) {
            OkragWyborczy badanyOkrag = okregi.getOkragWyborczy(i);
            int zmianaSumyWazonejBadanego = liczZmianeSumWazonych(badanyOkrag, super.getNazwaPartii(), dzialanie);
            int koszt = dzialanie.policzKoszt(badanyOkrag);

            if (zmianaSumyWazonejBadanego > wybranaZmianaSumyWazonej &&
                koszt <= budzet) {
                wybranaZmianaSumyWazonej = zmianaSumyWazonejBadanego;
                wybraneDzialanie = dzialanie;
                wybranyOkrag = badanyOkrag;
            }
        }
    }

    // maksymalizuje działanie i okrąg w sposób zachłanny i wykonuje to działanie
    // na wybranym okręgu, gdy żaden okrąg nie może już być wybrany wartość
    // 'wybranyOkrag' jest nullem
    private void znajdzIWykonajDzialanie(Okregi okregi, Dzialania dzialania) {
        int liczbaDzialan = dzialania.liczbaDzialan();
        for (int i = 0; i < liczbaDzialan; i++) {
            sprawdzJednoDzialanie(okregi, dzialania.getDzialanieNr(i));
        }

        if (wybranyOkrag != null) {
            budzet -= wybraneDzialanie.policzKoszt(wybranyOkrag);
            wybranyOkrag.poddajOkragDzialaniu(wybraneDzialanie);
        }
    }

    // wybiera i wykonuje działanie, którekolwiek jest możliwe do wykonania
    @Override
    public void przeprowadzKampanie(Dzialania dzialania, Okregi okregi) {
        do {
            resetujWybor();
            znajdzIWykonajDzialanie(okregi, dzialania);
        } while (wybranyOkrag != null);
    }
}
