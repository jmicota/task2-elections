package wybory.wyborca;
// Justyna Micota 418427

import wybory.okregi.Kandydat;
import wybory.okregi.ListaKandydatow;
import wybory.okregi.OkragWyborczy;
import wybory.partie.PojedynczeDzialanie;

import java.util.HashMap;

public class Wszechstronny extends Wyborca {
    protected final int[] bazoweWagi;

    public Wszechstronny(String imie, String nazwisko, int idOkregu, int[] wagi) {
        super(imie, nazwisko, idOkregu);
        bazoweWagi = new int[wagi.length + 1];
        bazoweWagi[0] = 0;
        System.arraycopy(wagi, 0, bazoweWagi, 1, wagi.length);
    }

    @Override
    public int liczSumaWazona(Kandydat kandydat) {
        int[] cechyKandydata = kandydat.getCechy();
        int licz = 0;

        for (int i = 1; i < bazoweWagi.length; i++) {
            licz += bazoweWagi[i] * cechyKandydata[i];
        }

        return licz;
    }

    @Override
    public int liczZmianeSumyWazonej(Kandydat kandydat, PojedynczeDzialanie dzialanie) {
        int[] wartosciZmian = dzialanie.getWartosciZmian();
        int[] cechyKandydata = kandydat.getCechy();
        int licz = 0;

        for (int i = 0; i < wartosciZmian.length; i++) {
            int nowaWartosc = bazoweWagi[i] + wartosciZmian[i];
            if (nowaWartosc > 100) {
                nowaWartosc = 100;
            }
            else if (nowaWartosc < -100) {
                nowaWartosc = -100;
            }

            nowaWartosc = Math.abs(nowaWartosc);
            int bezwzglednaBazowaWaga = Math.abs(bazoweWagi[i]);
            licz += (nowaWartosc - bezwzglednaBazowaWaga) * cechyKandydata[i];
        }

        return licz;
    }

    @Override
    public Kandydat wybierzKandydata(OkragWyborczy okragWyborczy) {
        int obecnyMaks = -100 * bazoweWagi.length;
        Kandydat wybrany = null;
        HashMap<String, ListaKandydatow> listy = okragWyborczy.getListyKandydatow();

        for (HashMap.Entry<String, ListaKandydatow> entry : listy.entrySet()) { // przejscie po wszystkich listach

            ListaKandydatow rozpatrywanaLista = entry.getValue();
            for (int i = 1; i < rozpatrywanaLista.dlugoscListy(); i++) { // po wszystkich kandydatach z listy
                Kandydat rozpatrywany = rozpatrywanaLista.getKandydat(i);
                int sumaWazona = liczSumaWazona(rozpatrywany);
                if (sumaWazona > obecnyMaks) {
                    wybrany = rozpatrywany;
                    obecnyMaks = sumaWazona;
                }
            }
        }

        return wybrany;
    }

    @Override
    public Kandydat wybierzKandydataZDwochOkregow(OkragWyborczy okragWyborczy1, OkragWyborczy okragWyborczy2) {
        Kandydat kandydat1 = wybierzKandydata(okragWyborczy1);
        Kandydat kandydat2 = wybierzKandydata(okragWyborczy2);

        int suma1 = liczSumaWazona(kandydat1);
        int suma2 = liczSumaWazona(kandydat2);

        if (suma1 >= suma2) {
            return kandydat1;
        }
        else return kandydat2;
    }

    // zwiększa bazową wagę pewnej cechy u wyborcy i pilnuje, aby
    // wartość bezwzględna nie przekroczyła 100
    private void dodajWartoscDoCechy(int wartosc, int nrCechy) {
        bazoweWagi[nrCechy] += wartosc;
        if (bazoweWagi[nrCechy] < -100) {
            bazoweWagi[nrCechy] = -100;
        }
        else if (bazoweWagi[nrCechy] > 100) {
            bazoweWagi[nrCechy] = 100;
        }
    }

    @Override
    public void poddajSieDzialaniu(PojedynczeDzialanie dzialanie) {
        int[] wartosciZmian = dzialanie.getWartosciZmian();
        for (int i = 0; i < wartosciZmian.length; i++) {
            dodajWartoscDoCechy(wartosciZmian[i], i);
        }
    }
}
