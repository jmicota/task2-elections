package wybory.wyborca;
// Justyna Micota 418427

import wybory.okregi.Kandydat;
import wybory.okregi.OkragWyborczy;
import wybory.partie.PojedynczeDzialanie;

public abstract class Wyborca {
    private final String imie;
    private final String nazwisko;
    private final int idOkregu;
    private Kandydat naKogoZaglosowal;

    public Wyborca(String imie, String nazwisko, int idOkregu) {
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.idOkregu = idOkregu;
        naKogoZaglosowal = null;
    }

    public int getIdOkregu() {
        return this.idOkregu;
    }

    public abstract int liczSumaWazona(Kandydat kandydat);

    public abstract int liczZmianeSumyWazonej(Kandydat kandydat, PojedynczeDzialanie dzialanie);

    protected abstract Kandydat wybierzKandydata(OkragWyborczy okragWyborczy);

    protected abstract Kandydat wybierzKandydataZDwochOkregow(OkragWyborczy okragWyborczy1,
                                                              OkragWyborczy okragWyborczy2);

    public abstract void poddajSieDzialaniu(PojedynczeDzialanie dzialanie);

    public void zaglosuj(OkragWyborczy okragWyborczy) {
        naKogoZaglosowal = wybierzKandydata(okragWyborczy);
        naKogoZaglosowal.dodajGlos();
    }

    public void zaglosujZDwoch(OkragWyborczy okragWyborczy1, OkragWyborczy okragWyborczy2) {
        System.out.println("in zaglosujZDwoch..");
        naKogoZaglosowal = wybierzKandydataZDwochOkregow(okragWyborczy1, okragWyborczy2);
        naKogoZaglosowal.dodajGlos();
    }

    public String toString() {
        String imieKandydata = naKogoZaglosowal.getImie();
        String nazwiskoKandydata = naKogoZaglosowal.getNazwisko();
        return (imie + " " + nazwisko + " " + imieKandydata + " " + nazwiskoKandydata + "\n");
    }
}
