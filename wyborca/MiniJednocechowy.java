package wybory.wyborca;
// Justyna Micota 418427

import wybory.okregi.Kandydat;
import wybory.okregi.ListaKandydatow;
import wybory.okregi.OkragWyborczy;
import wybory.partie.PojedynczeDzialanie;

import java.util.HashMap;

public class MiniJednocechowy extends Wyborca {
    protected final int ktoraCecha;

    public MiniJednocechowy(String imie, String nazwisko, int idOkregu, int ktoraCecha) {
        super(imie, nazwisko, idOkregu);
        this.ktoraCecha = ktoraCecha;
    }

    @Override
    public int liczSumaWazona(Kandydat kandydat) {
        return 0;
    }

    @Override
    public int liczZmianeSumyWazonej(Kandydat kandydat, PojedynczeDzialanie dzialanie) {
        return 0;
    }

    @Override
    public Kandydat wybierzKandydata(OkragWyborczy okragWyborczy) {
        HashMap<String, ListaKandydatow> listy = okragWyborczy.getListyKandydatow();
        int obecneMini = 101;
        Kandydat wybrany = null;

        for (HashMap.Entry<String, ListaKandydatow> entry : listy.entrySet()) { // przejscie po wszystkich listach
            ListaKandydatow lista = entry.getValue();
            int dlugoscListy = lista.dlugoscListy();

            for (int i = 1; i < dlugoscListy; i++) { // po wszystkich kandydatach
                Kandydat rozpatrywany = lista.getKandydat(i);
                int[] cechy = rozpatrywany.getCechy();
                int wartoscCechy = cechy[ktoraCecha];
                if (wartoscCechy < obecneMini) {
                    obecneMini = wartoscCechy;
                    wybrany = rozpatrywany;
                }
            }
        }
        return wybrany;
    }

    @Override
    public Kandydat wybierzKandydataZDwochOkregow(OkragWyborczy okragWyborczy1, OkragWyborczy okragWyborczy2) {
        Kandydat kandydat1 = wybierzKandydata(okragWyborczy1);
        Kandydat kandydat2 = wybierzKandydata(okragWyborczy2);

        int wartosc1 = kandydat1.getCechy()[ktoraCecha];
        int wartosc2 = kandydat2.getCechy()[ktoraCecha];

        if (wartosc1 <= wartosc2) {
            return kandydat1;
        }
        else return kandydat2;
    }

    @Override
    public void poddajSieDzialaniu(PojedynczeDzialanie dzialanie) {}
}
