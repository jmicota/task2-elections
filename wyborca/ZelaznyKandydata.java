package wybory.wyborca;
// Justyna Micota 418427

import wybory.okregi.Kandydat;
import wybory.okregi.ListaKandydatow;
import wybory.okregi.OkragWyborczy;
import wybory.partie.PojedynczeDzialanie;

import java.util.HashMap;

public class ZelaznyKandydata extends Wyborca {
    private final String nazwaPartii;
    private final int nrKandydata;

    public ZelaznyKandydata(String imie, String nazwisko, int idOkregu,
                            String nazwaPartii, int nrKandydata) {
        super(imie, nazwisko, idOkregu);
        this.nazwaPartii = nazwaPartii;
        this.nrKandydata = nrKandydata;
    }

    @Override
    public Kandydat wybierzKandydataZDwochOkregow(OkragWyborczy okragWyborczy1,
                                                  OkragWyborczy okragWyborczy2) {
        Kandydat wybrany;
        // wybiera kandydata ze swojego okręgu
        if (super.getIdOkregu() == okragWyborczy1.getIdOkregu()) {
            wybrany = wybierzKandydata(okragWyborczy1);
        }
        else {
            wybrany = wybierzKandydata(okragWyborczy2);
        }
        return wybrany;
    }

    @Override
    public void poddajSieDzialaniu(PojedynczeDzialanie dzialanie) {}

    @Override
    public int liczSumaWazona(Kandydat kandydat) {
        return 0;
    }

    @Override
    public int liczZmianeSumyWazonej(Kandydat kandydat, PojedynczeDzialanie dzialanie) {
        return 0;
    }

    @Override
    public Kandydat wybierzKandydata(OkragWyborczy okragWyborczy) {
        HashMap<String, ListaKandydatow> listy = okragWyborczy.getListyKandydatow();
        return listy.get(nazwaPartii).getKandydat(nrKandydata);
    }
}
