package wybory.wyborca;
// Justyna Micota 418427

import wybory.okregi.Kandydat;
import wybory.okregi.ListaKandydatow;
import wybory.okregi.OkragWyborczy;

import java.util.HashMap;

public class MiniJednocechowyJednopartyjny extends MiniJednocechowy {
    private final String nazwaPartii;

    public MiniJednocechowyJednopartyjny(String imie, String nazwisko,
                                         int idOkregu, String nazwaPartii, int ktoraCecha) {
        super(imie, nazwisko, idOkregu, ktoraCecha);
        this.nazwaPartii = nazwaPartii;
    }

    @Override
    public Kandydat wybierzKandydata(OkragWyborczy okragWyborczy) {
        HashMap<String, ListaKandydatow> listy = okragWyborczy.getListyKandydatow();
        int obecneMini = 101;
        Kandydat wybrany = null;

        ListaKandydatow listaPartii = listy.get(nazwaPartii);
        int dlugoscListy = listaPartii.dlugoscListy();

        for (int i = 1; i < dlugoscListy; i++) { // po wszystkich kandydatach
            Kandydat rozpatrywany = listaPartii.getKandydat(i);
            int wartoscCechy = rozpatrywany.getCechy()[ktoraCecha];
            if (wartoscCechy < obecneMini) {
                obecneMini = wartoscCechy;
                wybrany = rozpatrywany;
            }
        }
        return wybrany;
    }

    @Override
    public Kandydat wybierzKandydataZDwochOkregow(OkragWyborczy okragWyborczy1, OkragWyborczy okragWyborczy2) {
        Kandydat kandydat1 = wybierzKandydata(okragWyborczy1);
        Kandydat kandydat2 = wybierzKandydata(okragWyborczy2);

        int wartosc1 = kandydat1.getCechy()[ktoraCecha];
        int wartosc2 = kandydat2.getCechy()[ktoraCecha];

        if (wartosc1 <= wartosc2) {
            return kandydat1;
        }
        else return kandydat2;
    }
}
