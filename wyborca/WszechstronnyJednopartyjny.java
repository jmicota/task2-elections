package wybory.wyborca;
// Justyna Micota 418427

import wybory.okregi.Kandydat;
import wybory.okregi.ListaKandydatow;
import wybory.okregi.OkragWyborczy;

import java.util.HashMap;

public class WszechstronnyJednopartyjny extends Wszechstronny {
    private final String nazwaPartii;

    public WszechstronnyJednopartyjny(String imie, String nazwisko, int idOkregu,
                                      String nazwaPartii, int[] wagi) {
        super(imie, nazwisko, idOkregu, wagi);
        this.nazwaPartii = nazwaPartii;
    }

    @Override
    public Kandydat wybierzKandydata(OkragWyborczy okragWyborczy) {
        int obecnyMaks = Integer.MIN_VALUE, sumaWazona;
        Kandydat wybrany = null, rozpatrywany;
        HashMap<String, ListaKandydatow> listy = okragWyborczy.getListyKandydatow();
        ListaKandydatow listaPartii = listy.get(nazwaPartii);

        for (int i = 1; i < listaPartii.dlugoscListy(); i++) {
            rozpatrywany = listaPartii.getKandydat(i);
            sumaWazona = liczSumaWazona(rozpatrywany);

            if (sumaWazona > obecnyMaks) {
                wybrany = rozpatrywany;
                obecnyMaks = sumaWazona;
            }
        }
        return wybrany;
    }

    @Override
    public Kandydat wybierzKandydataZDwochOkregow(OkragWyborczy okragWyborczy1, OkragWyborczy okragWyborczy2) {
        Kandydat kandydat1 = wybierzKandydata(okragWyborczy1);
        Kandydat kandydat2 = wybierzKandydata(okragWyborczy2);

        int suma1 = liczSumaWazona(kandydat1);
        int suma2 = liczSumaWazona(kandydat2);

        if (suma1 >= suma2) {
            return kandydat1;
        }
        else return kandydat2;
    }
}
