package wybory.wyborca;
// Justyna Micota 418427

import wybory.okregi.Kandydat;
import wybory.okregi.ListaKandydatow;
import wybory.okregi.OkragWyborczy;
import wybory.partie.PojedynczeDzialanie;

import java.util.Random;

public class ZelaznyPartyjny extends Wyborca {
    private final String nazwaPartii;

    public ZelaznyPartyjny(String imie, String nazwisko, int idOkregu, String nazwaPartii) {
        super(imie, nazwisko, idOkregu);
        this.nazwaPartii = nazwaPartii;
    }

    @Override
    public int liczSumaWazona(Kandydat kandydat) {
        return 0;
    }

    @Override
    public int liczZmianeSumyWazonej(Kandydat kandydat, PojedynczeDzialanie dzialanie) {
        return 0;
    }

    @Override
    public Kandydat wybierzKandydata(OkragWyborczy okragWyborczy) {
        ListaKandydatow lista = okragWyborczy.getListyKandydatow().get(nazwaPartii);
        int rozmiarListy = lista.dlugoscListy();
        Random r = new Random();
        int wybrany = r.nextInt(rozmiarListy);
        while (wybrany == 0) {
            wybrany = r.nextInt(rozmiarListy);
        }
        return lista.getKandydat(wybrany);
    }

    @Override
    public Kandydat wybierzKandydataZDwochOkregow(OkragWyborczy okragWyborczy1, OkragWyborczy okragWyborczy2) {
        Random r = new Random();
        int ktoraLista = r.nextInt(2);
        if (ktoraLista == 0) {
            return wybierzKandydata(okragWyborczy1);
        }
        else return wybierzKandydata(okragWyborczy2);
    }

    @Override
    public void poddajSieDzialaniu(PojedynczeDzialanie dzialanie) {}
}
