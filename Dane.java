package wybory;
// Justyna Micota 418427

import wybory.okregi.Kandydat;
import wybory.okregi.OkragWyborczy;
import wybory.okregi.Okregi;
import wybory.partie.*;
import wybory.wyborca.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

// klasa implementująca obsługę pliku wejściowego
public class Dane {
    private final File file;
    // tablice przechowujące dane do utworzenia obiektów w odpowiednim momencie
    private int[] doScalenia;
    private String[] nazwyPartii;
    private int[] budzetyPartii;

    public Dane(File file) {
        this.file = file;
        doScalenia = new int[0];
        nazwyPartii = new String[0];
        budzetyPartii = new int[0];
    }

    private void inicjujTablice(int liczbaOkregow, int liczbaPartii) {
        this.nazwyPartii = new String[liczbaPartii];
        this.budzetyPartii = new int[liczbaPartii];
        this.doScalenia = new int[liczbaOkregow + 1];
        for(int i = 0; i < doScalenia.length; i++) {
            doScalenia[i] = i;
        }
    }

    private boolean jestCyfra(char c) {
        return ((int)c >= 48 && (int)c <= 58);
    }

    // dla kolejnych napisów interpretuje napis pomiędzy nawiasem
    // a przecinkiem i zmienia go w liczbę reprezentującą numer okręgu
    private void wczytajParyDoScalenia(Scanner scanner) {
        int liczbaPar = scanner.nextInt();
        for (int i = 0; i < liczbaPar; i++) {
            String para = scanner.next();
            int pierwszyOkrag = 0, drugiOkrag;

            // interpretacja liczby
            int p = 1;
            while (jestCyfra(para.charAt(p))) {
                pierwszyOkrag += Character.getNumericValue(para.charAt(p));
                pierwszyOkrag *= 10;
                p++;
            }
            pierwszyOkrag = pierwszyOkrag/10;

            drugiOkrag = pierwszyOkrag + 1;

            doScalenia[pierwszyOkrag] = drugiOkrag;
            doScalenia[drugiOkrag] = pierwszyOkrag;
        }
    }

    private void wczytajInformacjeOPartiach(int liczbaPartii, Scanner scanner) {
        // zapisywanie nazw partii do tablicy pobranych danych o partiach
        for (int i = 0; i < liczbaPartii; i++) {
            String nazwaPartii = scanner.next();
            nazwyPartii[i] = nazwaPartii;
        }

        // zapisywanie budzetow partii na odpowiadających ich nazwom pozycjach
        for (int i = 0; i < liczbaPartii; i++) {
            int budzetPartii = scanner.nextInt();
            budzetyPartii[i] = budzetPartii;
        }
    }

    private void wczytajIDeklarujPartie(Scanner scanner, SpisPartii spisPartii,
                                        int liczbaPartii) {
        // pobieranie danych o typie partii i deklaracja partii
        for (int i = 0; i < liczbaPartii; i++) {
            String typPartii = scanner.next();
            Partia partia = null;
            switch (typPartii) {
                case "R":
                    partia = new PartiaZRozmachem(nazwyPartii[i], budzetyPartii[i]);
                    break;
                case "S":
                    partia = new PartiaSkromna(nazwyPartii[i], budzetyPartii[i]);
                    break;
                case "W": // deklaracja własnej partii
                    partia = new PartiaNiesamodzielna(nazwyPartii[i], budzetyPartii[i]);
                    break;
                case "Z":
                    partia = new PartiaZachlanna(nazwyPartii[i], budzetyPartii[i]);
                    break;
            }
            spisPartii.dodajPartie(partia);
        }
    }

    private void wczytajOkregi(Okregi okregiWyborcze, int liczbaOkregow,
                               Scanner scanner, SpisPartii spisPartii) {
        for (int i = 0; i < liczbaOkregow; i++) {
            int liczbaWyborcow = scanner.nextInt();
            OkragWyborczy okragWyborczy = new OkragWyborczy(liczbaWyborcow, i + 1, spisPartii);
            okregiWyborcze.dodajOkrag(okragWyborczy);
        }
    }

    private Kandydat wczytajKandydata(Scanner scanner, int liczbaCech) {
        String imie = scanner.next();
        String nazwisko = scanner.next();
        scanner.nextInt();
        String nazwaPartii = scanner.next();
        int pozycjaNaLiscie = scanner.nextInt();
        int[] cechy = new int[liczbaCech + 1];

        for (int i = 1; i < liczbaCech + 1; i++) {
            cechy[i] = scanner.nextInt();
        }
        return new Kandydat(imie, nazwisko, nazwaPartii, pozycjaNaLiscie, cechy);
    }

    private void wczytajKandydatowOkregu(int nrOkregu, Okregi okregiWyborcze, Scanner scanner,
                                         int liczbaPartii, int liczbaCech) {
        OkragWyborczy okragWczytywany = okregiWyborcze.getOkragWyborczy(nrOkregu);
        int liczbaMandatow = okragWczytywany.liczbaMandatow();

        for (int i = 0; i < liczbaPartii; i++) {
            String nazwaPartii = nazwyPartii[i];

            for (int k = 0; k < liczbaMandatow; k++) {
                Kandydat kandydat = wczytajKandydata(scanner, liczbaCech);
                okragWczytywany.dodajKandydata(kandydat, nazwaPartii);
            }
        }
    }

    // wczytuje oraz deklaruje wyborcę
    private Wyborca wczytajWyborce(Scanner scanner, int liczbaCech) {
        String imie = scanner.next();
        String nazwisko = scanner.next();
        int nrOkregu = scanner.nextInt();
        int typWyborcy = scanner.nextInt();
        String nazwaPartii;
        int pozycjaNaLiscie, nrCechy;
        int[] bazoweWagi = new int[liczbaCech];

        Wyborca wyborca = null;
        switch(typWyborcy) {
            case 1:
                nazwaPartii = scanner.next();
                wyborca = new ZelaznyPartyjny(imie, nazwisko,
                                            nrOkregu, nazwaPartii);
                break;
            case 2:
                nazwaPartii = scanner.next();
                pozycjaNaLiscie = scanner.nextInt();
                wyborca = new ZelaznyKandydata(imie, nazwisko, nrOkregu,
                                            nazwaPartii, pozycjaNaLiscie);
                break;
            case 3:
                nrCechy = scanner.nextInt();
                wyborca = new MiniJednocechowy(imie, nazwisko,
                                            nrOkregu, nrCechy);
                break;
            case 4:
                nrCechy = scanner.nextInt();
                wyborca = new MaxiJednocechowy(imie, nazwisko,
                                            nrOkregu, nrCechy);
                break;
            case 5:
                for (int i = 0; i < liczbaCech; i++) {
                    bazoweWagi[i] = scanner.nextInt();
                }
                wyborca = new Wszechstronny(imie, nazwisko, nrOkregu,
                                            bazoweWagi);
                break;
            case 6:
                nrCechy = scanner.nextInt();
                nazwaPartii = scanner.next();
                wyborca = new MiniJednocechowyJednopartyjny(imie, nazwisko,
                                            nrOkregu, nazwaPartii, nrCechy);
                break;
            case 7:
                nrCechy = scanner.nextInt();
                nazwaPartii = scanner.next();
                wyborca = new MaxiJednocechowyJednopartyjny(imie, nazwisko,
                                            nrOkregu, nazwaPartii, nrCechy);
                break;
            case 8:
                for (int i = 0; i < liczbaCech; i++) {
                    bazoweWagi[i] = scanner.nextInt();
                }
                nazwaPartii = scanner.next();
                wyborca = new WszechstronnyJednopartyjny(imie, nazwisko,
                                        nrOkregu, nazwaPartii, bazoweWagi);
                break;
        }

        return wyborca;
    }

    // wczytuje wyborców jednego okręgu i umieszcza ich na liście wyborców
    // w tym okręgu
    public void wczytajWyborcowOkregu(int nrOkregu, Okregi okregiWyborcze,
                                      Scanner scanner, int liczbaCech) {
        OkragWyborczy okragWczytywany = okregiWyborcze.getOkragWyborczy(nrOkregu);
        int liczbaWyborcow = okragWczytywany.liczbaMandatow() * 10;

        for (int i = 0; i < liczbaWyborcow; i++) {
            Wyborca wyborca = wczytajWyborce(scanner, liczbaCech);
            okragWczytywany.dodajWyborce(wyborca);
        }
    }

    public PojedynczeDzialanie wczytajDzialanie(int liczbaCech, Scanner scanner) {
        int[] wartosciZmian = new int[liczbaCech];
        for (int i = 0; i < liczbaCech; i++) {
            wartosciZmian[i] = scanner.nextInt();
        }
        return new PojedynczeDzialanie(wartosciZmian);
    }

    // metoda pobierająca wszystkie dane z pliku
    // zwraca obiekt klasy symulacja ze wszystkimi danymi gotowymi do
    // przeprowadzenia symulacji
    public Symulacja pobierzDane() throws FileNotFoundException {
        int liczbaOkregow = 0, liczbaPartii = 0, liczbaDzialan = 0, liczbaCech = 0;
        Scanner scanner = new Scanner(file);

        if (scanner.hasNextLine()) {
            liczbaOkregow = scanner.nextInt();
            liczbaPartii = scanner.nextInt();
            liczbaDzialan = scanner.nextInt();
            liczbaCech = scanner.nextInt();
        }
        inicjujTablice(liczbaOkregow, liczbaPartii);

        wczytajParyDoScalenia(scanner);
        wczytajInformacjeOPartiach(liczbaPartii, scanner);

        SpisPartii spisPartii = new SpisPartii();
        wczytajIDeklarujPartie(scanner, spisPartii, liczbaPartii);

        Okregi okregiWyborcze = new Okregi(spisPartii);
        wczytajOkregi(okregiWyborcze, liczbaOkregow, scanner, spisPartii);

        for (int i = 1; i <= liczbaOkregow; i++) {
            wczytajKandydatowOkregu(i, okregiWyborcze, scanner,
                                    liczbaPartii, liczbaCech);
        }

        for (int i = 1; i <= liczbaOkregow; i++) {
            wczytajWyborcowOkregu(i, okregiWyborcze, scanner, liczbaCech);
        }

        Dzialania dzialania = new Dzialania(liczbaDzialan);
        for (int i = 0; i < liczbaDzialan; i++) {
            PojedynczeDzialanie dzialanie = wczytajDzialanie(liczbaCech, scanner);
            dzialania.dodajDzialanie(i, dzialanie);
        }

        for (int i = 1; i < liczbaOkregow; i++) {
            int zKtorymScalony = doScalenia[i];
            if (i != zKtorymScalony) {
                okregiWyborcze.getOkragWyborczy(i).zapiszZKtorymScalony(zKtorymScalony);
                okregiWyborcze.getOkragWyborczy(zKtorymScalony).zapiszZKtorymScalony(i);
            }
        }

        return new Symulacja(okregiWyborcze, dzialania, spisPartii);
    }
}
